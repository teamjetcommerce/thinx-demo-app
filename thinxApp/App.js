import * as React from 'react';
import { View, Text, TouchableOpacity,SafeAreaView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { WebView } from 'react-native-webview';
function HomeScreen(props) {
  return (
    <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onPress={()=>{
      props.navigation.navigate('WebViewScreen')
    }}>
      <Text>Open</Text>
    </TouchableOpacity>
  );
}

function WebViewScreen() {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor:"white" }}>

<WebView source={{ uri: 'https://thinx.customerdesk.io/' }} 
originWhitelist={['*']}
javaScriptEnabled={true}
            />
    </SafeAreaView>
  );
}

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{
        headerShown: false,
      }}>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="WebViewScreen" component={WebViewScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
